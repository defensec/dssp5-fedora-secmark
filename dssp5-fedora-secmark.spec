Summary: Bash script to manage dssp5-fedora secmark
Name: dssp5-fedora-secmark
Version: 0.1
Release: 1
License: Unlicense
Group: System Environment/Tools
Source: dssp5-fedora-createimages.tgz
URL: https://github.com/DefenSec/dssp5-fedora-secmark
Requires: nftables dssp5-fedora policycoreutils
BuildRequires: argbash make python3-docutils
BuildArch: noarch

%description
Bash script to manage dssp5-fedora secmark.

%prep
%autosetup -n dssp5-fedora-secmark

%build

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.gz
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%changelog
* Mon Apr 26 2021 Dominick Grift <dominick.grift@defensec.nl> - 0.1-1
- Initial package
